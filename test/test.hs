{-# LANGUAGE RecordWildCards #-}

module Main where
import GTA.Data.JoinList
import GTA.Core hiding (items)
import Test.Framework  ( defaultMain, testGroup )
import Test.Framework.Providers.HUnit ( testCase ) 
import Test.HUnit ( (@=?) )

tests = [testGroup "Simple Test Group" [
                        testCase "simple test" test_knapsack
                       ]]

test_knapsack = knapsack w items @=? AddIdentity(40)

prop_knapsack w items = knapsack w items == AddIdentity (knapsack' w items)

main :: IO ()
main = flip catch print $ defaultMain tests

weightlimit w = (<=w) <.> ws
  where ws = JoinListAlgebra{..} where 
           x1 `times` x2  = (   x1 +    x2) `min` (w+1)
           single i  = getWeight i `min` (w+1)
           nil = 0

knapsack w items = 
  subs items 
    >== weightlimit w
    >=> maxsumWith getValue

getWeight (w, v) = w
getValue (w, v) = v
items = [(1, 10), (4, 20), (2,30)]
w = 5

subs' [] = [[]]
subs' (a:x) = let ss = subs' x in map (a:) ss ++ ss
knapsack' w items = maximum $ map (sum.map getValue)$ filter ((<=w) . sum . map getWeight) $ subs' items

