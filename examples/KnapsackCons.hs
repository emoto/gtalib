{-# LANGUAGE RecordWildCards #-}

module Main where
import GTA.Data.ConsList
import GTA.Core hiding (items)

import System.Random

{- Demonstration

:l *KnapsackCons.hs

subs [(1, 30), (2, 20), (2, 40)] `aggregateBy` result

subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `aggregateBy` result
subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `aggregateBy` maxsumsolutionWith getValue

items <- randomItems 200 200
length items

:set +s
subs items `filterBy` weightlimit 200 `aggregateBy` maxsumsolutionWith getValue

subs items `aggregateBy` count


items <- randomItems 400 200
length items
subs items `filterBy` weightlimit 200 `aggregateBy` maxsumsolutionWith getValue

items <- randomItems 800 200
length items
subs items `filterBy` weightlimit 200 `aggregateBy` maxsumsolutionWith getValue

items <- randomItems 1600 200
length items
subs items `filterBy` weightlimit 200 `aggregateBy` maxsumsolutionWith getValue



-}

-- a naive program looking like a exponential cost program
knapsack capa items = 
  subs items 
    `filterBy` weightlimit capa
    `aggregateBy` maxsumsolutionWith getValue


--main function to check the heacy computation
main = do
  let w = 200
  let n = 20000
  items <- randomItems n w
  putStrLn $ "w=" ++ show w ++ ", #items = " ++ show n
  putStrLn.show $ knapsack w items

{-

ghc Knapsack.hs -rtsopts -O2

time ./Knapsack 

-}

{-
Selecting a generator: 

segs [1,2,3] `aggregateBy` result
inits [1,2,3] `aggregateBy` result
tails [1,2,3] `aggregateBy` result
subs [1,2,3] `aggregateBy` result
assigns "TF" [1,2,3] `aggregateBy` result


subs [(1, 30), (2, 20), (2, 40)] `aggregateBy` result
-}

{-
Designing a predicate (tester)
-}
-- the base definition of user-defined predicate to check the weight limit
-- Please define it as a composition of lightweight function + foldr
weightlimit' w = (<=w) . foldr f e where 
   f a x = getWeight a + x
   e = 0
{-
weightlimit' 3 $ [(1,30), (2,20)]
weightlimit' 3 $ [(1,30), (2,20), (2,40)]
-}

-- now, make the range of the foldr as small as possible
weightlimit'' w = (<=w) . foldr f e where 
   f a x = (getWeight a + x) `min` (w+1)
   e = 0
{-
weightlimit'' 3 $ [(1,30), (2,20)]
weightlimit'' 3 $ [(1,30), (2,20), (2,40)]
-}

{-
Finally, rewrite it MACHINICALLY:
    (composition)  .    -> <.>
    (foldr)        foldr-> foldr'
-}
weightlimit w = (<=w) <.> foldr' f e where 
   f a x = (getWeight a + x) `min` (w+1)
   e = 0

{-
--Now, we can use the tester in our GTA program.

subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `aggregateBy` result
-}

{-
Selecting an aggregator: 

-- knapsack problem
subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `aggregateBy` maxsumsolutionWith getValue

subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `aggregateBy` maxsumsolutionWith (\i -> let v = getValue i in if v == 40 then 0 else v)

-}

{-
Modified knapsack problem solvers via aggregators

-- counting the number of valid selections
subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `aggregateBy` count

-- k-best knapsack
subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `aggregateBy` maxsumsolutionKWith 3 getValue

-}



{-
Modified knapsack problem solvers via testers
-}

-- user-defined predicate for divisable-by-3
multipleOf k = (==0) <.> foldr' f e where
  f _ x = (1 + x) `mod` k
  e = 0
{-
-- with an additional condition: # of selected items are divisible by 3.
subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `filterBy` multipleOf 3 `aggregateBy` maxsumsolutionWith getValue

subs [(1, 30), (1, 10), (1, 10), (2, 20), (2, 40)] `filterBy` weightlimit 3 `filterBy` multipleOf 3 `aggregateBy` maxsumsolutionWith getValue

-}

-- user-defined predicate: at most one value item
oneValueItem v = (<=1) <.> foldr' f e where
  f i x = ((if getValue i >= v then 1 else 0) + x) `min` 2
  e = 0 

{-
subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `filterBy` oneValueItem 30 `aggregateBy` maxsumsolutionWith getValue

-}

-- user-defined predicate: descending in weights

descending :: (Ord w) => 
              (Maybe (w, Bool) -> Bool,
               ConsListAlgebra (w, v) (Maybe (w, Bool)))
descending = maybe True (\(_, t) -> t) <.> foldr' f e where
  e = Nothing
  f i x = let w = getWeight i 
          in maybe (Just (w, True)) (\(wr, t) -> (Just (w, t && wr > w))) x

{-
subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `filterBy` descending `aggregateBy` maxsumsolutionWith getValue

subs [(2, 20), (2, 40), (1, 30)] `filterBy` weightlimit 3 `filterBy` descending `aggregateBy` maxsumsolutionWith getValue

-}


-- =============== for designing a new generator ===========
{-

let x = bagOfNil `bagUnion` bagOfNil
x
let y = crossCons 1 x
y
crossCons 2 (y `bagUnion` x)


-}

--straightforward imlementation of subs generator on lists:
subs'' :: [a] -> Bag (ConsList a)
subs'' x = ss x where
    ss [] = bagOfNil
    ss (a:x) = crossCons a (bagOfNil `bagUnion` ss x)

{-
subs'' [1,2,3]

-}

--abstracted version of subs'':
subs' :: [a] -> ConsSemiring a s -> s
subs' x (GenericSemiring {..}) = ss x where
    ss [] = nil
    ss (a:x) = cons a (nil `oplus` ss x)
    ConsListAlgebra {..} = algebra
    CommutativeMonoid {..} = monoid

{-
subs' [1,2,3] freeSemiring
subs' [1,2,3] `aggregateBy` result

-}


-- computes the best value only
knapsackValue capa items = 
  subs items 
    `filterBy` weightlimit capa
    `aggregateBy` maxsumWith getValue

-- another notation
knapsackValue' w items = 
  subs items 
    >== weightlimit w
    >=> maxsumWith getValue

getWeight (w, v) = w
getValue (w, v) = v
exampleItems = [(1, 10), (4, 20), (2,30)]
examplelimit = 5

{-
user-defined aggregator
-}
--maxvalue:: Semiring (Int, Int) (Maybe Int)
maxvalue = GenericSemiring{monoid=CommutativeMonoid {..}, 
                           algebra=ConsListAlgebra {..}} where
    a `oplus` b = a `max` b
    identity    = Nothing
    cons a x    = maybe Nothing (\x -> Just (getValue a + x)) x
    nil         = Just 0

{-

subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `aggregateBy` maxvalue
subs [(1, 30), (2, 20), (2, 40)] `filterBy` weightlimit 3 `aggregateBy` maxsumWith getValue


-}


{-
other versions:

subs items >=> result
subs items >== weightlimit w >=> result 
subs items >== weightlimit w >=> result
subs items >== weightlimit w >=> count 
subs items >== weightlimit w >=> maxsumWith getValue
subs items >== weightlimit w >=> maxsumsolutionWith getValue
subs items >== weightlimit w >=> maxsumsolutionKWith 2 getValue
subs items >== weightlimit w >=> maxsumsolutionXKWith count 2 getValue

-}



-- with an additional condition: # of selected items are divisible by 3.
knapsack3 w items =
  subs items 
    `filterBy` weightlimit w 
    `filterBy` multipleOf 3
    `aggregateBy` maxsumsolutionWith getValue


randomItems :: Int -> Int -> IO ([(Int, Int)])
randomItems n w = do 
  setStdGen (mkStdGen 0)  -- always the same random sequence
  rand <- getStdGen
  let genItems (v:w:rs) = (v, w):genItems rs
  return (take n (genItems (randomRs (1, w) rand)))


