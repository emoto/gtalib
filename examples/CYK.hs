{-# LANGUAGE RecordWildCards,RankNTypes  #-}

module CYK where

import GTA.Core
import GTA.Util.GenericSemiringStructureTemplate
import Data.Map (singleton, assocs)

import GTA.Data.BinTree

{-
-- CKY parsing/matrix chain O(n^3)
-}


-- matrix chain

drawLVTree t = drawTree' t
    where
      drawTree' (NodeLV l r) = 
          let a' = "+"
              ls = drawTree' l
              rs = drawTree' r
              ln = length ls
              rn = length rs
              an = length a'
              cs = ls ++ rs
              ms = div (an-1) 2 + 1 -- position of the edge to the lower child
              ns = 1 -- distance between the parent and the upper child 
              rep :: forall a.Int -> a -> [a]
              rep n x = take n (repeat x)
              ds = (a'++rep ns '-'):rep (ln-1) (rep (an-ms) ' ' ++ ('|':rep (ns-1+ms) ' '))
              fs = (rep (an-ms) ' ' ++ ('+':rep (ns-1+ms) '-')):rep (rn-1) (rep (ns+an) ' ')
          in zipWith (++) (ds++fs) cs
      drawTree' (LeafLV a) = [show a]

instance (Show a) => Show (LVTree a) where
  showsPrec _ x s = unlines ("":drawLVTree x) ++ s

data MinCost = Cost (Int, (Int, Int))
             | IdentityOfMinCost
               deriving (Show, Eq, Ord, Read)
mincost = GenericSemiring {..} where
  monoid = CommutativeMonoid {..} where
    oplus IdentityOfMinCost b = b
    oplus a IdentityOfMinCost = a
    oplus (Cost a) (Cost b) = Cost (a `min` b)
    identity = IdentityOfMinCost
  algebra = LVTreeAlgebra {..} where
    nodeLV IdentityOfMinCost r = IdentityOfMinCost 
    nodeLV l IdentityOfMinCost = IdentityOfMinCost 
    nodeLV (Cost (c1, (m1, n1))) (Cost (c2, (m2, n2))) =
        Cost (c1+c2+m1*n1*n2, (m1, n2))
             -- assumption: n1 == m2
    leafLV a = Cost (0, a)

-- the distributivity holds in the restricted case: 
--  nodeLV ((c1, (m, n)) `min` (c2, (m, n))) r = 
--     nodeLV (c1, (m, n) r `min` nodeLV (c2, (m, n)) r
-- this restriction holds while the computation.

mincostsolution :: GenericSemiring (LVTreeAlgebra (Int, Int)) (MinCost, Bag (LVTree (Int, Int)))
mincostsolution = GenericSemiring {..} where
  s = freeSemiring :: GenericSemiring (LVTreeAlgebra (Int, Int)) (Bag (LVTree (Int, Int)))
  monoid = CommutativeMonoid {..} where
    oplus (a, x) (b, y) 
      = case compare a b of
          EQ -> (a, x `oplus'` y)
          GT -> (b, y)
          LT -> (a, x)
    identity = (IdentityOfMinCost, identity')
  algebra = pairAlgebra maxMonoSumAlgebra algebra'
  maxMonoSumAlgebra = let GenericSemiring {..} = mincost in algebra
  (monoid', algebra') = let GenericSemiring {..} = s in (monoid, algebra)
  (oplus', identity') = let CommutativeMonoid {..} = monoid' in(oplus, identity)


-- the matrix chain problem solver
matrixchain x = lvtrees x >=> mincostsolution

matrices = [(6,4),(4,5),(5,4)]

one = LVTreeMapFs {leafLVF = const 1}


{- for demonstration

lvtrees [(6,4),(4,5),(5,4)] >=> result
lvtrees [(6,4),(4,5),(5,4)] >=> sumproductBy one 
lvtrees [(6,4),(4,5),(5,4)] >=> mincost
lvtrees (take 50 (repeat (2,2))) >=> sumproductBy one
lvtrees (take 50 (repeat (2,2))) >=> mincost


-}

-- CYK parsing

drawTree dl dn t = drawTree' t
    where
      drawTree' (BinNode a l r) = 
          let a' = let o = dn a in if o == [] then "+" else o
              ls = drawTree' l
              rs = drawTree' r
              ln = length ls
              rn = length rs
              an = length a'
              cs = ls ++ rs
              ms = div (an-1) 2 + 1 -- position of the edge to the lower child
              ns = 1 -- distance between the parent and the upper child 
              rep :: forall a.Int -> a -> [a]
              rep n x = take n (repeat x)
              ds = (a'++rep ns '-'):rep (ln-1) (rep (an-ms) ' ' ++ ('|':rep (ns-1+ms) ' '))
              fs = (rep (an-ms) ' ' ++ ('+':rep (ns-1+ms) '-')):rep (rn-1) (rep (ns+an) ' ')
          in zipWith (++) (ds++fs) cs
      drawTree' (BinLeaf a) = [dl a]

instance (Show n, Show l) => Show (BinTree n l) where
  showsPrec _ x s = unlines ("":drawTree show show x) ++ s

drawTrees (Bag l) = putStr.unlines.concatMap (++[[]]).map (drawTree dl dn) $ l
  where 
    dn m = show m
    dl (m, a) = (show m ++ show a)



{-
traverseLtoR (Node _ l r) = traverseLtoR l ++ traverseLtoR r
traverseLtoR (Leaf (Left (a, m))) = [a]
--check l = (\(Bag x) -> and(map ((==l).traverseLtoR) x)) $ (trees [B,L,R] l >=> result)
-}
{-
Balanced Bracket Grammar
B is the starting symbol. 
 B -> BB
 B -> LR
 L -> LB
 R -> BR
 L -> '['
 R -> ']'

This grammar is ambiguous, e.g., "[[]]" has the following two derivations.
B-L'['
+-R-B-L'['
  | +-R']'
  +-R']'

B-L-L'['
| +-B-L'['
|   +-R']'
+-R']'

-}
data NonTerminal = B | L | R deriving (Show, Eq, Ord, Read)

validProduction = (==Just B) <.> BinTreeAlgebra{..} where 
  binLeaf (L, '[') = Just L
  binLeaf (R, ']') = Just R
  binLeaf _ = Nothing
  binNode s (Just l) (Just r) 
    = case (s, l, r) of
        (B, B, B) -> Just B
        (B, L, R) -> Just B
        (L, L, B) -> Just L
        (R, B, R) -> Just R
        otherwise -> Nothing
  binNode _ _ _ = Nothing
  
  
test = parse "[[]]" 

--validParses s = assignTrees [B,L,R] [B,L,R] s >== validProduction 
validParses s = lvtrees s >=< assignTrans [B,L,R] [B,L,R] >== validProduction

parses s = validParses s >=> result

parse s = drawTrees (parses s)
{-
In the above function 'parse', invalid trees are not computed because of the laziness. Thus, it is very fast, though the computation of invalid trees is very heavy (exponential)).
-}
countParse s = validParses s >=> count


-- maxProduct by preferences
prefs = BinTreeMapFs {..}
  where 
    binLeafF (m, a) = case m of 
                        B -> AddIdentity 1.0
                        L -> AddIdentity 1.0
                        R -> AddIdentity 1.0
    binNodeF p = case p of 
                   (B, B, B) -> AddIdentity 1.0
                   (B, L, R) -> AddIdentity 1.0
                   (L, L, B) -> AddIdentity 1.0
                   (R, B, R) -> AddIdentity 0.5

validProduction' = (==Just B) <.> BinTreeAlgebra{..} where 
  binLeaf (L, '[') = Just L
  binLeaf (R, ']') = Just R
  binLeaf _ = Nothing
  binNode s (Just l) (Just r) 
      = case (s, l, r) of
          ((B, B, B), B, B) -> Just B
          ((B, L, R), L, R) -> Just B
          ((L, L, B), L, B) -> Just L
          ((R, B, R), B, R) -> Just R
          otherwise -> Nothing
  binNode _ _ _ = Nothing

gen' = assignTrees [B,L,R] [(B,B,B),(B,L,R),(L,L,B),(R,B,R)]

maxPrefParse s = gen' s >== validProduction' >=> maxprodsolutionKBy 3 prefs

