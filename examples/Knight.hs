#!/usr/bin/env runhaskell
{-# LANGUAGE NoImplicitPrelude, RecordWildCards, FlexibleInstances, TypeSynonymInstances #-}
{-# OPTIONS -Wall #-}

{- Example GTA program to solve Knight's move problem: 
   Find a way to move a knight from a corner of a board to the opposite corner.

   The original code is 
     Knight.hs (http://qiita.com/items/a372458d171e373285b1) by @nushio .

-}

module Main where
import Data.Maybe
import Data.Tensor.TypeLevel
import GTA.Data.JoinList
import GTA.Core hiding (items)
import NumericPrelude
import Test.QuickCheck

knightMoves :: [Vec2 Int]
knightMoves =  [Vec :~ x :~ y | x <- [-2..2], y<-[-2..2], x^2 + y^2 == 5]

canMoveTo :: Vec2 Int -> Vec2 Int -> Bool               
canMoveTo a b = (a - b) `elem` knightMoves

bdSize :: Int
bdSize = 8

maxStep :: Int
maxStep = 7

knightSeq' :: JoinList (Vec2 Int) -> Bool
knightSeq' = isJust . ws
  where
    ws Nil = Just Nothing
    ws (Single r) = Just $ Just (r,r)
    ws (x1 `Times` x2) = do
      a1 <- ws x1
      a2 <- ws x2
      case (a1, a2) of
        (Nothing, _) -> return a2
        (_, Nothing) -> return a1
        (Just (r0,r1),Just (r2,r3)) 
          | canMoveTo r1 r2 -> return $ Just (r0,r3)
          | otherwise       -> Nothing


knightSeq :: (Maybe a -> Bool, JoinListAlgebra (Vec2 Int) (Maybe (Maybe (Vec2 Int, Vec2 Int))))
knightSeq = (isJust) <.> ws
  where
    ws = JoinListAlgebra{..} where
      nil = Just Nothing
      single r = Just $ Just (r,r)
      x1 `times` x2 = do
        a1 <- x1
        a2 <- x2
        case (a1, a2) of
          (Nothing, _) -> return a2
          (_, Nothing) -> return a1
          (Just (r0,r1),Just (r2,r3)) 
            | canMoveTo r1 r2 -> return $ Just (r0,r3)
            | otherwise       -> Nothing


knightSeq2 :: (Maybe a -> Bool, JoinListAlgebra (Vec2 Int, t) (Maybe (Maybe (Vec2 Int, Vec2 Int))))
knightSeq2 = (isJust) <.> knightSeq2Hom

knightSeq2Hom :: JoinListAlgebra (Vec2 Int, t) (Maybe (Maybe (Vec2 Int, Vec2 Int)))
knightSeq2Hom = JoinListAlgebra{..} where
      nil = Just Nothing
      single (r,_) = Just $ Just (r,r)
      x1 `times` x2 = do
        a1 <- x1
        a2 <- x2
        case (a1, a2) of
          (Nothing, _) -> return a2
          (_, Nothing) -> return a1
          (Just (r0,r1),Just (r2,r3)) 
            | canMoveTo r1 r2 -> return $ Just (r0,r3)
            | otherwise       -> Nothing

instance Arbitrary (Vec2 Int)
  where 
    arbitrary = oneof [return $ Vec :~ x :~ y | x<- [1..bdSize], y<-[1..bdSize]]

-- check of the associativity and the identity    
check_knightSeq2 :: IO ()
check_knightSeq2 = (quickCheck $ prop_Associativity knightSeq2Hom) 
                   >> (quickCheck $ prop_Identity knightSeq2Hom)


main :: IO ()
main = do
  putStr $ pprint2$ assignsBy genSigns [1..maxStep]  
    `filterBy` knightSeq2 
    `aggregateBy` result 
  return ()
    where 
    genSigns n | n == 1       = [(Vec :~ 1 :~ 1)]
               | n == maxStep = [(Vec :~ bdSize :~ bdSize)]
               | otherwise    = [Vec :~ x :~ y| x<- [1..bdSize], y<-[1..bdSize]]


pprint :: Bag (JoinList (Vec2 Int)) -> String
pprint (Bag xs) = unlines $ map (unwords . map (\ (Vec :~ x :~ y) -> show x ++ "," ++ show y) . dejoinize) xs

pprint2:: Bag (JoinList (Vec2 Int,Int)) -> String
pprint2(Bag xs) = unlines $ map (unwords . map (\ ((Vec :~ x :~ y),_) -> show x ++ "," ++ show y) . dejoinize) xs

