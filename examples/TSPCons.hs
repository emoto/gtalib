import GTA.Core;
import GTA.Data.ConsList;
import qualified Data.IntSet as IntSet
import System.Environment

-- checks whether a given list of edges is a path or not
isPath = ok <.> foldr' f e where
    ok = maybe False (\_ -> True)
    e = Just Nothing
    f _ Nothing = Nothing
    f ((v,_),_) (Just Nothing) = Just $ Just v
    f ((v,u),_) (Just (Just w)) | u == w = Just $ Just v
                                | True   =  Nothing
-- checks whether a given list of edges is a simple cycle of length n or not
spans n = ok <.> foldr' f e where
    e = IntSet.empty
    f ((v,_), _) x = IntSet.insert v x
    ok x = IntSet.size x == n

-- generates a list of edges from 1 to 1 including non-simple cycles.
genEdgeList n = assignsBy (edges n) [1..n]
edges n m | m == 1 = [(1, k) | k <- [2..n]]
          | m == n = [(k, 1) | k <- [2..n]]
          | True   = [(k, l) | k <- [2..n], l <- [2..n], not (k==l)]

-- TSP solver
tsp dist n = genEdgeList n
             `filterBy` isPath
             `filterBy` spans n
             `aggregateBy` maxsumsolutionWith (revOrd . dist . fst)

-- The answer is 1 -> 2 -> ... -> n -> 1 (and its reverse)
lineardist (m, n) | m == n - 1 = 1
                  | n == m - 1 = 1
                  | m < n = n - m + 1
                  | True  = m - n + 1

main = do a <- getArgs
          let n | length a > 0 = read $ head a
                | True         = 11
          putStrLn $ "n = " ++ show n
          print $ tsp lineardist n

{-
ghc TSPCons.hs -o TSPCons -O2 -rtsopts

time ./TSPCons 11

-}

{- 
-----------
Discussion
-----------

The size of the range of foldr of isPath is n+2.
The size of the range of foldr of spans n is 2^n

Therefore, the size of a table is O(n2^n) .

The number of signs assigned to an element by assignsBy is O(n^2).

The assignsBy updates the table O(n) times.

In total, O(n^4 2^n) algorithm.

This is worse than the well-known DP of TSP by a factor of O(n^2) (perhaps). The reason is that we generate a list of edges so that we can compute the minimum sum by the given aggregator. We might be able to define a new aggregator to avoid this factor. 

-}



