#!/usr/bin/env runhaskell
{-# LANGUAGE NoImplicitPrelude, RecordWildCards #-}
{-# OPTIONS -Wall #-}
{- 
  ConsList version of Knight.hs .
  
 -}

module Main where
import Data.Maybe
import Data.Tensor.TypeLevel
import GTA.Data.ConsList
import GTA.Core hiding (items)
import NumericPrelude



knightMoves :: [Vec2 Int]
knightMoves =  [Vec :~ x :~ y | x <- [-2..2], y<-[-2..2], x^2 + y^2 == 5]

canMoveTo :: Vec2 Int -> Vec2 Int -> Bool               
canMoveTo a b = (a - b) `elem` knightMoves

bdSize :: Int
bdSize = 8

maxStep :: Int
maxStep = 7

-- Tester is essentially a composition of a function and a foldr: ok . foldr
knightSeq2 :: (Maybe a -> Bool, ConsListAlgebra (Vec2 Int, t) (Maybe (Maybe (Vec2 Int))))
knightSeq2 = (isJust) <.> foldr' f (Just Nothing)
  where
      f (r,_) x2 = do
        a2 <- x2
        case a2 of
          Nothing -> return (Just r)
          Just r2 
            | canMoveTo r r2 -> return $ Just r
            | otherwise      -> Nothing

main :: IO ()
main = do
  putStr $ pprint2$ assigns' genSigns [1..maxStep]  
    `filterBy` knightSeq2 
    `aggregateBy` result 
  return ()
    where 
    genSigns n | n == 1       = [(Vec :~ 1 :~ 1)]
               | n == maxStep = [(Vec :~ bdSize :~ bdSize)]
               | otherwise    = [Vec :~ x :~ y| x<- [1..bdSize], y<-[1..bdSize]]

{-
 modified 'assings'  (the same as 'assignsBy' implemented in the library)
 We can generate a set of signs depending on the given element
-} 
assigns' :: (a -> [m]) -> [a] -> ConsSemiring (m,a) s -> s
assigns' f x (GenericSemiring {..}) = foldr cons' nil x
    where cons' a y = foldr oplus identity [cons (m, a) y | m <- f a]
          ConsListAlgebra {..} = algebra
          CommutativeMonoid {..} = monoid

pprint :: Bag (ConsList (Vec2 Int)) -> String
pprint (Bag xs) = unlines $ map (unwords . map (\ (Vec :~ x :~ y) -> show x ++ "," ++ show y) . deconsize) xs

pprint2:: Bag (ConsList (Vec2 Int,Int)) -> String
pprint2(Bag xs) = unlines $ map (unwords . map (\ ((Vec :~ x :~ y),_) -> show x ++ "," ++ show y) . deconsize) xs

